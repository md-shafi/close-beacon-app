package se.hel.closebeaconapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class BeaconListView extends AppCompatActivity {

//    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String SERIAL_NUM_MSG = "se.ruh.blescannerlite3.SERIAL_NUM_MSG";
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view_beacon);

        listView = (ListView) findViewById(R.id.beacons_list);
        final Intent intent = getIntent();

        ArrayList<String> scanResultHex = intent.getStringArrayListExtra(ScanActivity.EXTRA_MSG);
        ArrayList<String> scanResultInt = new ArrayList<>();

        for(String r : scanResultHex){
            scanResultInt.add(hexToIntString(r));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,android.R.id.text1, scanResultInt);

        // assign adapter to the listview
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {

                                                // ListView Clicked item index
                                                int itemPositon = i;

                                                // ListView clicked item value
                                                String itemValue = (String) listView.getItemAtPosition(itemPositon);
                                                startBeaconConfig(itemValue);
                                                Toast.makeText(getApplicationContext(), "Beacon Selected : " +itemValue , Toast.LENGTH_LONG).show();

                                            }
                                        }
        );
    }

    public void startBeaconConfig(String serialNum){
//        BeaconConf.textViewSerial.setText(serialNum);
//        finish();
        Intent intent1 = new Intent(this, BeaconConf.class);
        intent1.putExtra(SERIAL_NUM_MSG, serialNum);
        startActivity(intent1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    public void refresh(MenuItem item) {
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }

    public String hexToIntString(String bleDeviceHexString){
        StringBuilder bleDeviceIntString = new StringBuilder();
        String[] hexArr = bleDeviceHexString.split(":");
        for (int i=0; i<hexArr.length; i++){
            Integer temp = Integer.parseInt(hexArr[i].trim(), 16);
            if (temp < 100){
                bleDeviceIntString.append("0");
            }
            bleDeviceIntString.append(temp.toString());
            if (i==hexArr.length-1){
                continue;
            }
            else{
                bleDeviceIntString.append("-");
            }
        }
        return bleDeviceIntString.toString();
    }
}