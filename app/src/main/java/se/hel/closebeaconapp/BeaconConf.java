package se.hel.closebeaconapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;


public class BeaconConf extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "se.hel.closebeaconapp.EXTRA_MESSAGE";
    public static EditText editTextUUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conf_beacon);
        editTextUUID = (EditText) findViewById(R.id.uuid_num);
        Intent intent = getIntent();
        String serialNum = intent.getStringExtra(BeaconListView.SERIAL_NUM_MSG);
        TextView textView = (TextView) findViewById(R.id.text_Serial_num);
        textView.setText(serialNum);
        textView.setTextColor(0xFF00FF00);
    }

    public void genUUID(View view){
        String[] uuid = new String[3];
        Intent intent = new Intent(this, UuidListView.class);
        for (int i=0; i<uuid.length; i++){
            uuid[i] = UUID.randomUUID().toString().replaceAll("-","").substring(0,16).trim();
        }
        intent.putExtra(EXTRA_MESSAGE, uuid);
        startActivity(intent);
    }

    public void activateBeacon(View view){
        Toast.makeText(getApplicationContext(), "Could not connect to close beacon", Toast.LENGTH_LONG).show();
    }
}
