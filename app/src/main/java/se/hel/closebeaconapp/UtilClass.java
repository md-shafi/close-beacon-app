package se.hel.closebeaconapp;

import android.content.Context;

import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.future.ResponseFuture;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class UtilClass
{
    static private final String baseURL = "http://smartsensor.io/";
    static private final String our_authcode = "AHGc6KSONfL7";
    static private final String server_charset = "ISO-8859-1";
    private static final byte[] version = { (byte) 1, (byte) 0, (byte) 0, (byte) 0 };
    private static final int TIMEOUT_IN_SECONDS = 10;
    private static String foo;

    public static void main(String[] args)
    {
        /*
        try
        {
            System.out.println("Bad request returns: " + validAthentication(our_authcode.replace("7", "8")));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try
        {
            System.out.println("Good request returns: " + validAthentication(our_authcode));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        */
    }

    private static String sha1auth(byte[] request, String response) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");

        byte[] one = request;
        byte[] two = response.getBytes(server_charset);
        byte[] combined = new byte[one.length + two.length];

        for (int i = 0; i < combined.length; ++i)
        {
            combined[i] = i < one.length ? one[i] : two[i - one.length];
        }

        byte[] result = mDigest.digest(combined);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    /*
     * Returns true/false depending on whether or not the sent auth-code was
     * valid or not Throws exceptions in cases that something goes wrong
     * (presumably with the POST request to the auth server)
     */
    public static boolean validAthentication(String authcode, Context context) throws Exception
    {

        //Create request and encrypted request string
        byte[] request = createAuthByteArray(authcode);
        String encryptedreq = new RSAEncrypter(context).encrypt(request);

        ResponseFuture<String> bar = Ion.with(context).
                load("GET", baseURL+"CBtest/auth_user.php?enc="+(java.net.URLEncoder.encode(encryptedreq, "UTF-8")))
                        .asString();

        foo = bar.get();

        //Is this a good authcode?
        if (foo.substring(1).equals(sha1auth(request, "OK"))) return true;

        //Otherwise, the authcode is bad or encryption failed. Regardless, do not let user into app.
        return false;
    }



    @SuppressWarnings("unused")
    private static byte[] createAuthByteArray(String authcode) throws UnsupportedEncodingException
    {
        byte[] ourbytearray = new byte[20];
        // Version number 1
        ourbytearray[0] = version[0];
        ourbytearray[1] = version[1];
        ourbytearray[2] = version[2];
        ourbytearray[3] = version[3];

        int x = 4;
        for (byte e : authcode.getBytes("ISO-8859-1"))
        {
            ourbytearray[x] = e;
            x++;
        }

        // Add random numbers for the last 4 bytes

        Random random = new Random();
        byte[] temp = new byte[1];
        for (; x != 20; x++)
        {
            random.nextBytes(temp);
            ourbytearray[x] = temp[0];
        }

        return ourbytearray;
    }
}
