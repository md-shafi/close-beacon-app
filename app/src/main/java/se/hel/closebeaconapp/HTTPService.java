package se.hel.closebeaconapp;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HTTPService {
    @GET("CBtest/getpubkey.php")
    Call<ResponseBody> getPubKey();

    @GET("CBtest/auth_user.php")
    Call<ResponseBody> doAuth(@Query("enc") String authstring);
}
