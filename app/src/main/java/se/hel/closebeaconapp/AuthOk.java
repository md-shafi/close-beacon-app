package se.hel.closebeaconapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AuthOk extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ok_auth);
    }

    public void scanBeacon(View view){
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }


}
