package se.hel.closebeaconapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class StarterActivity extends AppCompatActivity {

    public static final String AppPref = "app_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences mSharedPreferences = getSharedPreferences(AppPref, Context.MODE_PRIVATE);
        Intent intent;
        boolean chkPref = mSharedPreferences.getBoolean(AppPref, false);
        if (chkPref){
            intent = new Intent(this, AuthOk.class);
        }
        else {
            intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);
        finish();
    }

}
