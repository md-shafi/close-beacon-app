package se.hel.closebeaconapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String BASE_URL = "http://smartsensor.io/";
    public static final String TAG2 = MainActivity.class.getSimpleName();
    private SharedPreferences sharedPreferences;

    //private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void login(View view) {
        EditText editText = (EditText) findViewById(R.id.auth_code);
        String authCode = editText.getText().toString();
        System.out.println(authCode);

        try {
            if (UtilClass.validAthentication(authCode, getApplicationContext())){
                Toast.makeText(getApplicationContext(), "Login Successful.", Toast.LENGTH_LONG).show();
                sharedPreferences = getSharedPreferences(StarterActivity.AppPref, Context.MODE_PRIVATE);
                sharedPreferences.edit().putBoolean(StarterActivity.AppPref, true).apply();
                Intent intent = new  Intent (this, AuthOk.class);
                startActivity(intent);
                finish();
            }
            else {
                Toast.makeText(getApplicationContext(), "Authorization failed. Please Try Again", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
