package se.hel.closebeaconapp;

import android.content.Context;
import android.util.Base64;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.koushikdutta.ion.future.ResponseFuture;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.ExecutionException;

import javax.crypto.Cipher;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RSAEncrypter {

    // Server hosting the public key
    static private final String baseURL = "http://smartsensor.io/";
    private static final int TIMEOUT_IN_SECONDS = 20;
    private String publicKey="";
    private byte[] keyAsByte;
    private static String foo;

    private static String getPubKeyFromWeb(Context context) throws IOException, InterruptedException, ExecutionException {
        foo=null;
        System.out.println("Starting Ion req");
        ResponseFuture<String> bar = Ion.with(context).load("GET", baseURL+"CBtest/getpubkey.php").asString();
                /*
                Ion.with(context).
                load("GET", baseURL+"CBtest/getpubkey.php").
                asString().
                setCallback(new FutureCallback<String>() {
            //@Override

            public void onCompleted(Exception e, String heh) {
                // chirp chirp
                foo = heh;
                foo = foo.replace("-----BEGIN PUBLIC KEY-----", "");
                foo = foo.replace("-----END PUBLIC KEY-----", "");
                foo = foo.replace("\n", "");
            }});
            */



/*
        System.out.println("Wait 4 Ion req2 compllete");
        int x = 0;
        while (bar.isDone() != true) {
            Thread.sleep(1000);
            x++;
            //if (foo != null) break;
            if (x == TIMEOUT_IN_SECONDS)
                throw new IOException("Getting public key failed; timed out after " + TIMEOUT_IN_SECONDS + " seconds. Network down?");
        }
*/

        foo = bar.get();
        foo = foo.replace("-----BEGIN PUBLIC KEY-----", "");
        foo = foo.replace("-----END PUBLIC KEY-----", "");
        foo = foo.replace("\n", "");
        System.out.println("Ion req done: " + foo);
        return foo;
    }

    public RSAEncrypter(Context context) throws IOException, InterruptedException {
        try {
            publicKey = getPubKeyFromWeb(context);
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            //this.keyAsByte = java.util.Base64.getDecoder().decode(publicKey);
            this.keyAsByte = Base64.decode(publicKey, Base64.DEFAULT);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    public String encrypt(byte[] input) throws Exception {
        X509EncodedKeySpec key = new X509EncodedKeySpec(keyAsByte);
        KeyFactory factory = KeyFactory.getInstance("RSA");

        PublicKey pubKey = factory.generatePublic(key);
        Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] output = cipher.doFinal(input);
        //URLEncoder.encode(stringtobeencoded, "UTF-8")
        //return java.util.Base64.getEncoder().encodeToString(output);
        return Base64.encodeToString(output, Base64.DEFAULT);
    }
}