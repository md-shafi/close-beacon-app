package se.hel.closebeaconapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class UuidListView extends AppCompatActivity {
    public static String EXT_MSG = "se.hel.closebeaconapp.EXT_MSG";
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view_uuid);

        listView = (ListView) findViewById(R.id.uuid_list);
        final Intent intent = getIntent();

        // Defined Array values to show in ListView
        String[] values = intent.getStringArrayExtra(BeaconConf.EXTRA_MESSAGE);

        for (String val : values) {
            System.out.println(val);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        // assign adapter to the listview
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                                                // ListView Clicked item index
                                                int itemPositon = i;

                                                // ListView clicked item value
                                                String uuid = (String) listView.getItemAtPosition(itemPositon);

                                                selectUUID(uuid);

                                                // Show Alert
                                                Toast.makeText(getApplicationContext(), "UUID Selected : " + uuid, Toast.LENGTH_LONG).show();

                                            }
                                        }
        );
    }

    public void selectUUID(String uuid) {
        BeaconConf.editTextUUID.setText(uuid);
        finish();
    }

}
